root = global ? window

root.Meteor.publish 'allPosts', 
	->
		root.Posts.find({})

root.Meteor.publish 'singlePost',
	(id)->
		root.Posts.find(id)