root = global ? window

root.Meteor.methods
	submitPost: (post) ->
		user = Meteor.user()
		if not user
			throw new Meteor.Error 401, 'You need to log in first.'
		additionalParams = 
			author: 'Author'
			createdAt: new Date()
			userId: user._id

		_.extend(post, additionalParams)
		post._id = root.Posts.insert(post)
		post
