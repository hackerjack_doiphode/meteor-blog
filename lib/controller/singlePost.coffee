root = global ? window

root.SinglePostController = root.BaseController.extend
	template: 'postPage'
	waitOn: ->
		Meteor.subscribe 'singlePost', @params._id

	data: ->
		root.Posts.findOne(@params._id)