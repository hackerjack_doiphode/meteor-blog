root = global ? window

root.MainPageController = BaseController.extend
	template: 'postsList'
	findOptions: ->
		sort:
			createdAt: -1
	waitOn: ->
		Meteor.subscribe 'allPosts', @findOptions()
	data: ->
		posts:
			root.Posts.find({}, @findOptions())
