root = global ? window

root.Router.configure
	loadingTemplate: 'loading'
	notFoundTemplate: 'notFound'

root.Router.route '/',
	name: 'root'
	controller: 'MainPageController'

root.Router.route '/new',
	name: 'newPost'
	controller: 'NewPostController'

root.Router.onBeforeAction (->
	if not Meteor.user()
		if Meteor.loggingIn()
			@render @loadingTemplate
		else
			@render 'accessDenied'
	else
		@next()
	), only: 'newPost'

root.Router.route '/:_id',
	name: 'singlePost'
	controller: 'SinglePostController'

